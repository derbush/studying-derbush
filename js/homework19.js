"use strict";

// Exercise 1

let a = "$100";
let b = "300$";

let summ = Number(a.slice(1, 4)) + Number(b.slice(0, 3));

console.log(`Exercise 1: ${summ}`);


// Exercise 2

let message = '  привет, медвед    ';

message = message.trim();
message = message[0].toLocaleUpperCase() + message.slice(1);

console.log(`Exercise 2: ${message}`);


// Exercise 3

let age = prompt("Сколько Вам лет?");
let age_description;


if (age < 4) {
    age_description = "младенец";
}  else if (age < 12) {
     age_description = "ребёнок";
}  else if (age < 19) {
     age_description = "подросток";
}  else if (age < 41) {
     age_description = "познаёте жизнь";
}  else if (age < 81) {
     age_description = "познали жизнь";
}  else {
     age_description = "долгожитель";
}

alert(`Вам ${age} и Вы ${age_description}`)


// Exercise 4

let message1 = 'Я работаю со строками как профессионал!';

let count = message1.split(" ").length; // Divide string by spaces into words and count elements in array

console.log(`Exercise 4: There are ${count} words`);
