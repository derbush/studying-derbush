"use strict";


// Exerсise 1
let a = '100px';
let b = '323px';

let result = parseInt(a) + parseInt(b);

console.log("Exercise 1:", result);

// Exercise 2

let numbers = [ 10, -45, 102, 36, 12, 0, -1 ];

console.log("Exercise 2:", Math.max(...numbers));


// Exercise 3

let d = 0.111;
console.log("Exercise 3.1:", Math.ceil(d));

let e = 45.333333;
console.log("Exercise 3.2:", e.toFixed(1));


let f = 3;
console.log("Exercise 3.3:", f ** 5);

let g = 400000000000000;
console.log("Exercise 3.4:", g.toExponential());

let h='1' !== 1;
console.log("Exercise 3.5:", h);


// Exercise 4

console.log("Exercise 4:", 0.1+0.2===0.3); // Returns false, why?
/*Потому что используется оператор сравнения, а также числа с плавающей точкой,
которые в процессе преобразования из двоичной системы несут неточность.
Чтобы избежать этого можно использовать:*/
let num1 = 0.1;
let num2 = 0.2;

console.log("Exercise 4 answer:", (num1 * 100 + num2 * 100) / 100 === 0.3  );