"use strict";

// Exercise 1

/* Напишите цикл, который выведет в консоль все четные числаот 0 до 20 (включая число 20).
Можно использовать какwhile,так и циклfor */

console.log('Exercise 1.1');
let number = 0;
while (number <= 20 ) {
    console.log(number);
    number += 2;
}

console.log('\nExercise 1.2');
for (let number = 0; number <= 20; number += 2) {
    console.log(number);
}


// Exercise 2

/*Напишите скрипт, который три раза подряд спросит упользователя число, а просуммирует введённые значения.
Необходимо учесть тот случай, когда пользователь вводит не число, тогда скрипт должен показать сообщение 
“Ошибка, выввели не число” и закончить выполнен  */

console.log('\nExercise 2');
let result = 0;

function summator(result, promtedNum) {
    return result + Number(promtedNum);
}

for (let iter = 0; iter <3; iter++) {
    let promtedNum = prompt("Число?");
    if (Number(promtedNum)) {
        result = summator(result, promtedNum);
    } else {
        alert("Ошибка, вы ввели не число!"); break;
    }
}
console.log(`Сумма: ${result}`);


// Exercise 3

/*Напишите функциюgetNameOfMonth(month), которая будетпринимать порядковый номер месяца в году (от 0 до 11) и
возвращать его название на русском языке. Например, есливызватьgetNameOfMonth(0), то функция должна вернуть“Январь”
Затем, напишите цикл, который выведет в консоль названиевсех существующих месяцев, кроме октября.*/

console.log('\nExercise 3');
const arrMonths = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

function getNameOfMonth(month) {
    return `Месяц: ${arrMonths[month]}`;
}

let monthName = getNameOfMonth(prompt("Месяц?"));

console.log(monthName);

let month_el
console.log("\nМесяцы:");
for (month_el of arrMonths) {
    if (month_el !== "Октябрь") {
    console.log(month_el);
    }
}

// Exercise 4
/*Функция выполняющаяся сразу после того как была определена. Замыкает в себе область видимости.
Вызывать отдельно не требуется.

console.log('\nExercise 4');

(function () {
    console.log("test IIFE");
}());
*/
